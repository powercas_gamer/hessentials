package me.powercas_gamer.hessentials.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        if (e.getPlayer().hasPermission("hessentials.staff")) {
            Player player = e.getPlayer();
            e.setQuitMessage("§c§lSTAFF §r§7» §f" + player.getName() + " has left!");
        }
        if (e.getPlayer().hasPermission("hessentials.donator")) {
            Player player = e.getPlayer();
            e.setQuitMessage("§a§lDONATOR §r§7» §f " + player.getName() + " has left!");
        }
        if (e.getPlayer().hasPermission("hessentials.media")) {
            Player player = e.getPlayer();
            e.setQuitMessage("§d§lMEDIA §r§7» §f " + player.getName() + " has left!");
        }
    }
}