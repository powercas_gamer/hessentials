package me.powercas_gamer.hessentials.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

  @EventHandler
  public void onPlayerJoin (PlayerJoinEvent event) {
    if (event.getPlayer().hasPermission("hessentials.staff")) {
      Player player = event.getPlayer();
      event.setJoinMessage("§c§lSTAFF §r§7» §f" + player.getName() + " has joined!");
    }
    if (event.getPlayer().hasPermission("hessentials.donator")) {
      Player player = event.getPlayer();
      event.setJoinMessage("§a§lDONATOR §r§7» §f " + player.getName() + " has joined!");
    }
    if (event.getPlayer().hasPermission("hessentials.media")) {
      Player player = event.getPlayer();
      event.setJoinMessage("§d§lMEDIA §r§7» §f " + player.getName() + " has joined!");
    }
  }
}
