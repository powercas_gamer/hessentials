package me.powercas_gamer.hessentials;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.event.EventBus;
import me.powercas_gamer.hessentials.Commands.*;
import me.powercas_gamer.hessentials.Listeners.PlayerJoinListener;
import me.powercas_gamer.hessentials.Listeners.PlayerQuitListener;
import me.powercas_gamer.hessentials.Listeners.PlayerTeleportListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public final class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        System.out.println("[hEssentials] Loading plugin...");
        System.out.println("[hEssentials] Registering Commands...");
        this.registerCommands();
        System.out.println("[hEssentials] Successfully registered commands!");
       /* System.out.println("[hEssentials] Registering Config Files...");
        this.registerConfig();
        System.out.println("[hEssentials] Successfully registered config files!"); */
        System.out.println("[hEssentials] Registering Listeners...");
        this.registerListeners();
        System.out.println("[hEssentials] Successfully registered listeners!");
        System.out.println("[hEssentials] Registering APIs");
        this.registerAPIs();
        System.out.println("[hEssentials] Successfully registered APIs");
        System.out.println("[hEssentials] Successfully loaded plugin!");

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void registerCommands() {
        this.getCommand("hessentials").setExecutor(new hEssentialsCMD());
        this.getCommand("creative").setExecutor(new CreativeCMD());
        this.getCommand("survival").setExecutor(new SurvivalCMD());
        //this.getCommand("fly").setExecutor((CommandExecutor) new FlyCMD());
        this.getCommand("list").setExecutor(new ListCMD());
        this.getCommand("teleport").setExecutor(new TeleportCMD());
        this.getCommand("teleporthere").setExecutor(new TeleportHereCMD());
        this.getCommand("whois").setExecutor(new WhoisCMD());
    }

    /* uWu
    public void registerConfig() {
        FileConfiguration config = this.getConfig();

        this.saveDefaultConfig();
        this.saveConfig();
        this.getConfig();

    }
    */

    public void registerListeners() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(new PlayerQuitListener(), this);
        pm.registerEvents(new PlayerJoinListener(), this);
        pm.registerEvents(new PlayerTeleportListener(), this);
    }

    public void registerAPIs() {
        LuckPermsApi api = LuckPerms.getApi();
        EventBus eventBus = api.getEventBus();
    }
}
