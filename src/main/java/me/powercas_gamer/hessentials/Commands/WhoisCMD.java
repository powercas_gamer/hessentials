package me.powercas_gamer.hessentials.Commands;

import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.manager.GroupManager;
import me.lucko.luckperms.api.manager.UserManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class WhoisCMD  implements CommandExecutor {

    LuckPermsApi api = LuckPerms.getApi();
    UserManager userManager = api.getUserManager();
    GroupManager groupManager = api.getGroupManager();

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("hessentials.whois")) {
                sender.sendMessage("§cNo Permission");
                return false;
            } else {
                if (args.length == 0) {
                    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cUsage: &7/whois &b<player>"));
                } else {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target == null) {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                    } else {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f&m----------------------------------------------------"));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7» &ePlayer: &F" + target.getName()));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7» &eUUID: &f" + target.getUniqueId()));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7» &eGamemode: &f" + target.getGameMode()));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7» &eOperator: &f" + target.isOp()));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7» &eRank: &f" + userManager.getUser(args[0]).getPrimaryGroup()));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f&m----------------------------------------------------"));
                    }
                }
            }
        }
        return true;
    }
}
