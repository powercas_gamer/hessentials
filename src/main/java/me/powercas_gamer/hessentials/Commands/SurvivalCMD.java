package me.powercas_gamer.hessentials.Commands;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SurvivalCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("hessentials.survival")) {
                sender.sendMessage("§cNo Permissions.");
                return true;
            } else {
                sender.sendMessage("§eYour gamemode has been set to §cSurvival§e.");
                player.setGameMode(GameMode.SURVIVAL);
            }
        }
        return true;
    }
}