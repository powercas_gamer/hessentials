package me.powercas_gamer.hessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class ListCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {
        if ((sender instanceof Player) || (sender instanceof ConsoleCommandSender)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f&m----------------------------------------------------"));
            sender.sendMessage(ChatColor.AQUA + "There are currently " + ChatColor.BLUE + Bukkit.getOnlinePlayers().size() + ChatColor.GRAY + "/" + ChatColor.BLUE + Bukkit.getMaxPlayers() + ChatColor.AQUA + " player(s) online.");
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f&m----------------------------------------------------"));
        }
        else {
            sender.sendMessage("You need to be a player to execute this command!");

        }
        return true;
    }
}
