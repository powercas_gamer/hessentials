package me.powercas_gamer.hessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;


public class hEssentialsCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String l, String[] args) {
        if ((sender instanceof Player) || (sender instanceof ConsoleCommandSender)) {
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f&m----------------------------------------------------"));
            sender.sendMessage(ChatColor.AQUA + "This server is using " + ChatColor.BLUE + Bukkit.getPluginManager().getPlugin("hEssentials").getName() + ChatColor.AQUA + " Version " + Bukkit.getPluginManager().getPlugin("hEssentials").getDescription().getVersion() + ChatColor.AQUA + " By " + ChatColor.BLUE + "powercas_gamer");
            sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&f&m----------------------------------------------------"));
        } else {
            if (sender instanceof ConsoleCommandSender) {
                sender.sendMessage("You need to be a player to use this command.");
            }
        }
        return true;
    }
}
