package me.powercas_gamer.hessentials.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportCMD implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
        if ((sender instanceof Player)) {
            Player player = (Player) sender;
            if (!player.hasPermission("hessentials.teleport")) {
                sender.sendMessage("§cNo Permission");
                return true;
            } else {
                if (args.length < 1) {
                    sender.sendMessage("§cUsage: §7/teleport §b<player>");
                    return true;
                } else {
                    Player target = Bukkit.getPlayerExact(args[0]);
                    if (target == null) {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ePlayer with name or UUID '&f" + args[0] + "&e' not found."));
                    } else {
                        sender.sendMessage("§eTeleporting to §c" + target.getName() + "§e.");
                        ((Player) sender).teleport(target);
                    }
                }
            }
        }
        return true;
    }
}